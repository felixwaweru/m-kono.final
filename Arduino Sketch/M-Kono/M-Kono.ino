#include <Servo.h>


Servo thumb, index, middle, ring, pinky; // Define servo
int bend = 800;
int straight = 2200;
int half = 1800;
int wait = 500;
int pbend = 2200;
int pstraight = 800;
int phalf = 1300;
String phoneInput = "";

void setup() { 
  // Set servo to digital pins
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  
  Serial.println("--- Start Serial Monitor ---");
  Serial.println("--- Serial Connection Open ---");
  thumb.attach(5);  
  index.attach(6); 
  middle.attach(9);  
  ring.attach(10);  
  pinky.attach(11);  

  openHand();           
  delay(1000);        
  closeHand();           
  delay(1000);
  openHand();           
  delay(1000);        
  closeHand();           
  delay(1000);
  point();           
  delay(1000);        
  thumbsUp();           
  delay(1000);
} 

void loop() {            
// Loop through motion tests
  
  while (Serial.available() > 0 )
  {
    
    phoneInput=Serial.readString();
    action();
    
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(300);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(300);                       // wait for a second
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(300);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(300);
    //Serial.println("Command: " + phoneInput );
    
//    if (phoneInput = "open")
//    {
//    openHand();
//    Serial.println("Command: " + phoneInput );
//    phoneInput = "";
//    break;
//    }
//    
//    if (phoneInput = "close")
//    {
//    closeHand();
//    Serial.println("Command: " + phoneInput );
//    phoneInput = "";
//    break;
//    }
//    
//    if(phoneInput = "point")
//    {
//    point();
//    break;
//    }
//    
//    if(phoneInput = "good")
//    {
//    thumbsUp();
//    break;
//    }
  }         
}

//method used to translate phone input into actions
void action()
{
//Serial.println("Command: " + phoneInput );
  phoneInput= phoneInput;
  if(phoneInput = "open")
    {
    openHand();
    Serial.println("Command: " + phoneInput ); 
    }
  else if(phoneInput = "close")
    {
    closeHand();
    Serial.println("Command: " + phoneInput );
    }
  else if(phoneInput == "point")
    {
    point();
    }
  else if(phoneInput == "good")
    {
    thumbsUp();
    }
}


//method to drive individual fingers
void drive(Servo s, int pos)
{
  s.writeMicroseconds(pos);
}

//methods contianing
void openHand() {         
  drive(thumb, straight);
  drive(index, straight);
  drive(middle, straight);
  drive(ring, straight);
  drive(pinky, straight);
}

void closeHand() {   
  drive(pinky, pbend);
  drive(ring, bend);
  drive(middle, bend);      
  drive(index, bend);
  drive(thumb, bend);
}

void thumbsUp()
{
  drive(pinky, pbend);
  drive(ring, bend);
  drive(middle, bend);      
  drive(index, bend);
  drive(thumb, straight);
}
void point()
{
  drive(pinky, pbend);
  drive(ring, bend);
  drive(middle, bend);      
  drive(index, straight);
  drive(thumb, bend);
}
