# M-Kono

A project that entails the development of a prosthetic hand that makes use of an Android application to receive and convert vocal commands into actions.
The implementation of this project will rely on the use of an Arduino board, an Android application and a 3d printed prosthetic hand of which will work together to make a fully functional prosthetic arm that will receive input from the user and carry out the same functionalities as those of a human hand.