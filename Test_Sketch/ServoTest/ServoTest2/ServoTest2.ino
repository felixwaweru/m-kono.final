#include <Servo.h>
Servo pinky, ringF, middleF, indexF, thumb;
int bend = 800, straight = 2200, half = 1800, wait = 500, pbend = 2200, pstraight = 800, middlehalf = 1500, phalf = 1300;

void setup()
{
  pinky.attach(5);
  ringF.attach(6);
  middleF.attach(9);
  indexF.attach(10);
  thumb.attach(11);
  delay(500);
}

void loop()
{
  test();
}

void test()
{
  drive(pinky, pbend);
  drive(pinky, pstraight);
  drive(ringF, bend);
  drive(ringF, straight);
  drive(middleF, bend);
  drive(middleF, straight);
  drive(indexF, bend);
  drive(indexF, straight);
  drive(thumb, bend);
  drive(thumb, straight);
}

void drive(Servo s, int pos)
{
  s.writeMicroseconds(pos);
  delay(wait);
}
