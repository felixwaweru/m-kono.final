package mkono.com.m_kono;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class MainActivity extends AppCompatActivity {


    private TextView txtSpeechInput;
    TextView text;
    public final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    UsbManager usbManager;
    UsbDevice device;
    UsbSerialDevice serialPort;
    UsbDeviceConnection connection;
    private final int REQ_CODE_SPEECH_INPUT = 100;


    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver to automatically start and stop the Serial connection.
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            serialPort.setBaudRate(9600);
                            Toast.makeText(MainActivity.this, "Serial port ok", Toast.LENGTH_SHORT).show();
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);

                        } else {
                            checkConnections();
                            Toast.makeText(MainActivity.this, "Please check your connection and try again.", Toast.LENGTH_SHORT).show();
                            Log.d("SERIAL", "PORT NOT OPEN");
                        }
                    } else {
                        checkConnections();
                        Toast.makeText(MainActivity.this, "Please check your connection and try again.", Toast.LENGTH_SHORT).show();
                        Log.d("SERIAL", "PORT IS NULL");
                    }
                } else {
                    checkConnections();
                    Toast.makeText(MainActivity.this, "Please enable USB permissions and try again.", Toast.LENGTH_SHORT).show();
                    Log.d("SERIAL", "PERM NOT GRANTED");
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                checkConnections();
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                try
                {
                    serialPort.close();
                    Toast.makeText(MainActivity.this, "Connection Closed", Toast.LENGTH_SHORT).show();
                    disconnectNotification();
                }
                catch(Exception e)
                {
                    Toast.makeText(MainActivity.this, "Connection Closed", Toast.LENGTH_SHORT).show();
                    disconnectNotification();
                }

            }
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        checkConnections();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);
        txtSpeechInput = (TextView) findViewById(R.id.textView);
        ImageButton help = (ImageButton) findViewById(R.id.help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Help.class));
            }
        });
        ImageButton b = (ImageButton)findViewById(R.id.voice);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

    }

    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String command = result.get(0).toLowerCase();
                    Toast.makeText(MainActivity.this, "You have spoken the word " + command, Toast.LENGTH_SHORT).show();
                    if (command.equals("open") || command.equals("close") || command.equals("greet")
                            || command.equals("point") || command.equals("good") || command.equals("help")) {
                        txtSpeechInput.setText(result.get(0));
                        switch (command) {
                            case "open":
                                try {
                                    serialPort.write(command.getBytes());
                                    Toast.makeText(MainActivity.this, "Open hand", Toast.LENGTH_SHORT).show();
                                    break;
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Please check your connection to the device and try again.", Toast.LENGTH_SHORT).show();
                                    checkConnections();
                                    break;
                                }

                            case "close":
                                try {
                                    serialPort.write(command.getBytes());
                                    Toast.makeText(MainActivity.this, "Close hand", Toast.LENGTH_SHORT).show();
                                    break;
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Please check your connection to the device and try again.", Toast.LENGTH_SHORT).show();
                                    checkConnections();
                                    break;
                                }

                            case "greet":
                                try {
                                    serialPort.write(command.getBytes());
                                    Toast.makeText(MainActivity.this, "Greeting", Toast.LENGTH_SHORT).show();
                                    break;
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Please check your connection to the device and try again.", Toast.LENGTH_SHORT).show();
                                    checkConnections();
                                    break;
                                }
                            case "point":
                                try {
                                    serialPort.write(command.getBytes());
                                    Toast.makeText(MainActivity.this, "Pointing", Toast.LENGTH_SHORT).show();
                                    break;
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Please check your connection to the device and try again.", Toast.LENGTH_SHORT).show();
                                    checkConnections();
                                    break;
                                }
                            case "good":
                                try {
                                    serialPort.write(command.getBytes());
                                    Toast.makeText(MainActivity.this, "Good", Toast.LENGTH_SHORT).show();
                                    break;
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Please check your connection to the device and try again.", Toast.LENGTH_SHORT).show();
                                    checkConnections();
                                    break;
                                }
                            case "help":
                                startActivity(new Intent(MainActivity.this, Help.class));
                                break;
                        }
                        break;
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "Please speak a command from the available options and try again.", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        }
    }


    public void checkConnections() {
            HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
            if (usbDevices.isEmpty())
               {
                startActivity(new Intent(MainActivity.this, Pop.class));
                Toast.makeText(MainActivity.this, "Waiting for connection.", Toast.LENGTH_SHORT).show();
               }
            else if (!usbDevices.isEmpty()) {
                Toast.makeText(MainActivity.this, "Connected.", Toast.LENGTH_SHORT).show();
                boolean keep = true;
                for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                    device = entry.getValue();
                    int deviceVID = device.getVendorId();
                    if (deviceVID == 2341)//Arduino Vendor ID
                    {
                        PendingIntent pi = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                        usbManager.requestPermission(device, pi);
                        keep = false;
                        menuNotification();
                    } else {
                        connection = null;
                        device = null;
                        Toast.makeText(MainActivity.this, "Please check your device/connection and try again.", Toast.LENGTH_SHORT).show();
                    }

                    if (!keep)
                        break;
                }
            }
    }


    public void menuNotification()
    {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                .setContentTitle("M-Kono")
                .setVibrate(new long[]{ 1000 })
                .setLights(Color.CYAN, 3000, 3000)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentText("Connected to the prosthetic.");
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int color = 0x9B0A0D;
            mBuilder.setSmallIcon(R.drawable.splash2_transparent);
            mBuilder.setColor(color);
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(001, mBuilder.build());
    }

    public void disconnectNotification()
    {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                .setContentTitle("M-Kono")
                .setVibrate(new long[]{ 1000,1000 })
                .setLights(Color.CYAN, 3000, 3000)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentText("Disconnected from the prosthetic.");
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int color = 0x9B0A0D;
            mBuilder.setSmallIcon(R.drawable.splash2_transparent);
            mBuilder.setColor(color);
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(001, mBuilder.build());
    }
}


